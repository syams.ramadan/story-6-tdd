from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *

# Create your tests here.

class uniTests(TestCase):

    def test_url_exists(self):
        response = Client().get("/story7")
        self.assertEqual(response.status_code,200)

    def test_func_exec(self):
        response = resolve("/story7")
        self.assertEqual(response.func,index)

    def test_template_exists(self):
        response = Client().get("/story7")
        self.assertTemplateUsed(response,"story7.html")

    def test_button_exists(self):
        response = Client().get("/story7")
        content = response.content.decode("utf8")
        self.assertIn("<button",content)
