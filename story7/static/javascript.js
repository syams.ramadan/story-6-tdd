var counter = 1;

function changeTheme(){
  counter += 1

  if(counter == 1){
    $(".top").css('background-color','#6d7e96')
    $(".heading").css('color','#ebebeb')
    $(".accor-button").css('background-color','#6d7e96')
    $(".accor-button").css('color','#ebebeb')
  }

  if(counter == 2){
    counter = 0
    $(".top").css('background-color','#E3AFBC')
    $(".heading").css('color','#5D001E')
    $(".accor-button").css('background-color','#E3AFBC')
    $(".accor-button").css('color','#5D001E')

  }
}