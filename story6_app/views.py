from django.shortcuts import render, redirect
from .models import *
from .forms import *
from datetime import datetime

def homepageView(request):
    status = Status.objects.all()
    return render(request, 'homepage.html', {'status':status})
    
def StatusView(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            s = form.cleaned_data['status']
            status = Status(status=s, date=datetime.now(),)
            status.save()
            return redirect('homepage')
    else:
        form = StatusForm()
    return render(request,'status-page.html', {'form':form})
# Create your views here.
