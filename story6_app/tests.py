from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.views import *
from story6_app.forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from .views import *
import unittest

class UnitTest(TestCase):
    def test_homepage_url_is_resolved(self):
        self.assertEqual(resolve(reverse('homepage')).func, homepageView)
        
    def test_status_page_url_is_resolved(self):
        url = reverse('add status')
        self.assertEqual(resolve(url).func, StatusView)
        
    def test_homepage_GET(self):
        response = Client().get(reverse('homepage'))
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'homepage.html')
    
    def test_add_status_page_GET(self):
        response = Client().get(reverse('add status'))
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'status-page.html')
     
    def test_add_status_page_POST(self):
        response_post = Client().post('/status-page/',
            {'status' : "some status haha i love baba"}
        )
        self.assertEqual(response_post.status_code, 302)

    
    def test_models_can_add(self):
        Status.objects.create(status="bobo")
        sum = Status.objects.all().count()
        self.assertEqual(sum,1)
        self.assertNotEqual(sum,0)
    
    def create_status(self, status = "test_status"):
        return Status.objects.create(status= status)

    def test_model_create(self):
        test_model = self.create_status()
        self.assertTrue(isinstance(test_model, Status))
    
    def test_status_form(self):
        form = StatusForm(data={'status':"some status haha"})
        self.assertTrue(form.is_valid())
        s = form.cleaned_data['status']
        self.assertEqual(s,"some status haha")
        
    def test_status_form_no_input(self):
        form = StatusForm(data={'status':""})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
        
class Lab5FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super(Lab5FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.implicitly_wait(20)
        selenium.get('http://127.0.0.1:8000/status-page/')
        # find the form element
        selenium.implicitly_wait(20)
        title = selenium.find_element_by_id('id_status').send_keys('Coba Coba')      
        selenium.implicitly_wait(20)
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        # submitting the form
        submit.send_keys(Keys.RETURN)
        selenium.implicitly_wait(20)  
        status = selenium.find_element_by_class_name('status').text
        self.assertIn("Coba Coba", status)  
        selenium.implicitly_wait(20)

# Create your tests here.
