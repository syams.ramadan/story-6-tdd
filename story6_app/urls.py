from django.urls import path
from . import views

urlpatterns = [
    path('', views.homepageView, name='homepage'),
    path('status-page/', views.StatusView, name='add status'),
]